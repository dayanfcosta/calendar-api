# Calendar API

This is an application that emulates a calendar API, with few basic endpoints.

As I didn't have enough data from candidates and interviewers, I decided to leave their names as  
unique keys, it means that the API does not allow two candidates with the same name, neither two  
interviewers with the same name. As this is not a production ready application, I implemented just
the tests I thought were necessary, skipping the REST endpoints tests and basic tests.

## Technology stack

1. Java 11
2. Spring Boot
3. MongoDB
4. jUnit 5 and Mockito for unit tests
5. Docker and docker compose

## Getting started

To be able to run this project you must have docker, docker componse installed, and Java 11.  
Their installation guide are in: https://docs.docker.com/install/
, https://docs.docker.com/compose/install/,
and https://www.oracle.com/java/technologies/javase-jdk11-downloads.html, please visit their
websites.

## Running this project

**YOU MUST HAVE DOCKER, DOCKER COMPOSE AND JAVA 11 INSTALLED.**

1. Clone this repository or download its zip;
2. Go to the repository root folder and run the following command in your terminal:
   2.1. `docker-compose up -d`
3. This may take few minutes because it might be starting MongoDB and inserting some documents;
4. If you want to run the tests, you must delete the docker containers for MongoDB using the
   following commands:
   4.1. `docker-compose down`

## Endpoints:

The application has endpoints related to Candidates, Interviewers and Interviews. It is also
provided a postman collection with the available endpoints for each of these domains in the
folder `postman-collection`.

## Code

This code is structured based on Clean Architecture, with domain, infrastructure, use cases and rest
layers and it's reflected on the packages.

### Packages:

1. Domain:
   Contain all domain classes (Entities) and interfaces needed.
2. Infrastructure:
   Contain all the code related to the MongoDB connection and queries. Each subpackage has the
   representation of the document stored on the MongoDB and the repository implementation.
3. Use Case:
   Contain the use cases of each domain, like registering or fetching a candidate
4. REST:
   Here we have all the Controllers and JSON representations, as well configurations for global
   error handling.
