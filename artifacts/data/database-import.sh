#! /bin/bash

cd data

ls *.json | while read jsonfile; do \
  mongoimport --type json --file $jsonfile --host mongodb-calendar --db calendar \
  --drop --jsonArray; done
