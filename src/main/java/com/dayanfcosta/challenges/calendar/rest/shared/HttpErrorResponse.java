package com.dayanfcosta.challenges.calendar.rest.shared;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.util.Collection;
import java.util.Collections;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HttpErrorResponse {

    private final int statusCode;
    private final String error;
    private final String message;
    private final Instant timestamp;
    private final Collection<ValidationError> validationErrors;

    @JsonCreator
    public HttpErrorResponse(final int statusCode, final String error, final String message,
                             final Instant timestamp, final Collection<ValidationError> validationErrors) {
        this.statusCode = statusCode;
        this.error = error;
        this.message = message;
        this.timestamp = timestamp;
        this.validationErrors = validationErrors;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public Collection<ValidationError> getValidationErrors() {
        return validationErrors;
    }

    public static HttpErrorResponse of(final HttpStatus status, final String message) {
        return new HttpErrorResponse(status.value(), status.getReasonPhrase(), message, Instant.now(), Collections.emptyList());
    }

    public static HttpErrorResponse of(final HttpStatus status, final String message, final Collection<ValidationError> validationErrors) {
        return new HttpErrorResponse(status.value(), status.getReasonPhrase(), message, Instant.now(), validationErrors);
    }

}
