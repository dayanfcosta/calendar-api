package com.dayanfcosta.challenges.calendar.rest.interviewer.form;

import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerBuilder;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import com.dayanfcosta.challenges.calendar.rest.shared.InterviewSlotForm;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class NewInterviewerForm {

  private final String name;
  private final List<InterviewSlotForm> slots;

  @JsonCreator
  public NewInterviewerForm(final String name, final List<InterviewSlotForm> slots) {
    this.name = name;
    this.slots = slots;
  }

  public Interviewer toModel() {
    return new InterviewerBuilder()
        .withName(name)
        .withSlots(slotsModel())
        .build();
  }

  public String getName() {
    return name;
  }

  public List<InterviewSlotForm> getSlots() {
    return slots;
  }

  private Set<InterviewSlot> slotsModel() {
    return slots.stream()
        .map(slot -> new InterviewSlot(slot.getStart(), slot.getEnd(), false))
        .collect(Collectors.toCollection(LinkedHashSet::new));
  }
}
