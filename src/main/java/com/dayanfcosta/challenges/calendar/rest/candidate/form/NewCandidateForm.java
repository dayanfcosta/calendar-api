package com.dayanfcosta.challenges.calendar.rest.candidate.form;

import com.dayanfcosta.challenges.calendar.domain.candidate.Candidate;
import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateBuilder;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import com.dayanfcosta.challenges.calendar.rest.shared.InterviewSlotForm;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class NewCandidateForm {

  private final String name;
  private final List<InterviewSlotForm> slots;

  @JsonCreator
  public NewCandidateForm(final String name, final List<InterviewSlotForm> slots) {
    this.name = name;
    this.slots = slots;
  }

  public Candidate toModel() {
    return new CandidateBuilder()
        .withName(name)
        .withSlots(slotsModel())
        .build();
  }

  public String getName() {
    return name;
  }

  public List<InterviewSlotForm> getSlots() {
    return slots;
  }

  private Set<InterviewSlot> slotsModel() {
    return slots.stream()
        .map(slot -> new InterviewSlot(slot.getStart(), slot.getEnd(), false))
        .collect(Collectors.toCollection(LinkedHashSet::new));
  }
}
