package com.dayanfcosta.challenges.calendar.rest.interview.form;

import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateBuilder;
import com.dayanfcosta.challenges.calendar.domain.interview.Interview;
import com.dayanfcosta.challenges.calendar.domain.interview.InterviewBuilder;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerBuilder;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import com.dayanfcosta.challenges.calendar.rest.shared.InterviewSlotForm;

public class NewInterviewForm {

    private String candidate;
    private String interviewer;
    private InterviewSlotForm slot;

    NewInterviewForm() {
    }

    public NewInterviewForm(final String candidate, final String interviewer, final InterviewSlotForm slot) {
        this.candidate = candidate;
        this.interviewer = interviewer;
        this.slot = slot;
    }

    public String getCandidate() {
        return candidate;
    }

    public String getInterviewer() {
        return interviewer;
    }

    public InterviewSlotForm getSlot() {
        return slot;
    }

    public Interview toModel() {
        final var interviewer = new InterviewerBuilder().withName(getInterviewer()).build();
        final var candidate = new CandidateBuilder().withName(getCandidate()).build();
        return new InterviewBuilder()
                .withInterviewer(interviewer)
                .withCandidate(candidate)
                .withSlot(new InterviewSlot(slot.getStart(), slot.getEnd(), false))
                .build();
    }
}
