package com.dayanfcosta.challenges.calendar.rest.interviewer.form;

import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.rest.config.Views;
import com.dayanfcosta.challenges.calendar.rest.shared.InterviewSlotForm;
import com.fasterxml.jackson.annotation.JsonView;
import java.util.List;
import java.util.stream.Collectors;

public class InterviewerForm {

    @JsonView({Views.Register.class, Views.List.class})
    private String id;
    @JsonView({Views.Register.class, Views.List.class})
    private String name;
    @JsonView({Views.Register.class, Views.List.class})
    private List<InterviewSlotForm> slots;

    InterviewerForm() {
    }

    public InterviewerForm(final String id, final String name, final List<InterviewSlotForm> slots) {
        this.id = id;
        this.name = name;
        this.slots = slots;
    }

    public static InterviewerForm fromModel(final Interviewer candidate) {
        return new InterviewerForm(
                candidate.getId(),
                candidate.getName(),
                candidate.getSlots().stream()
                        .map(InterviewSlotForm::fromModel)
                        .collect(Collectors.toUnmodifiableList())
        );
    }
}
