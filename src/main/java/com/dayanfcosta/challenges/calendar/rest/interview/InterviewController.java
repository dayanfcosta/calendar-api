package com.dayanfcosta.challenges.calendar.rest.interview;

import com.dayanfcosta.challenges.calendar.rest.config.Views;
import com.dayanfcosta.challenges.calendar.rest.interview.form.InterviewForm;
import com.dayanfcosta.challenges.calendar.rest.interview.form.NewInterviewForm;
import com.dayanfcosta.challenges.calendar.usecase.interview.FindInterview;
import com.dayanfcosta.challenges.calendar.usecase.interview.InterviewRegistration;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/interviews", consumes = MediaType.APPLICATION_JSON_VALUE)
public class InterviewController {

    private final InterviewRegistration registrationUseCase;
    private final FindInterview findInterviewUseCase;

    public InterviewController(final InterviewRegistration registrationUseCase,
            final FindInterview findInterviewUseCase) {
        this.registrationUseCase = registrationUseCase;
        this.findInterviewUseCase = findInterviewUseCase;
    }

    @PostMapping
    @JsonView(Views.Register.class)
    @ResponseStatus(HttpStatus.CREATED)
    Mono<InterviewForm> register(@RequestBody final NewInterviewForm form) {
        return registrationUseCase.register(form.toModel()).map(InterviewForm::fromModel);
    }

    @JsonView(InterviewViews.ListInterviewer.class)
    @GetMapping("/interviewer/{name}")
    Flux<InterviewForm> interviewsForInterviewer(@PathVariable("name") final String name) {
        return findInterviewUseCase.findByInterviewer(name).map(InterviewForm::fromModel);
    }

    @JsonView({InterviewViews.ListCandidate.class})
    @GetMapping("/candidate/{name}")
    Flux<InterviewForm> interviewsForCandidate(@PathVariable("name") final String name) {
        return findInterviewUseCase.findByCandidate(name).map(InterviewForm::fromModel);
    }
}
