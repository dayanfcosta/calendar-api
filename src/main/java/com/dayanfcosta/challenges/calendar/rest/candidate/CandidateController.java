package com.dayanfcosta.challenges.calendar.rest.candidate;

import com.dayanfcosta.challenges.calendar.rest.candidate.form.CandidateForm;
import com.dayanfcosta.challenges.calendar.rest.candidate.form.NewCandidateForm;
import com.dayanfcosta.challenges.calendar.usecase.candidate.CandidateRegistration;
import com.dayanfcosta.challenges.calendar.usecase.candidate.FindCandidate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/candidates", consumes = MediaType.APPLICATION_JSON_VALUE)
public class CandidateController {

    private final CandidateRegistration registrationUseCase;
    private final FindCandidate findUseCase;

    public CandidateController(
            final CandidateRegistration registrationUseCase,
            final FindCandidate findUseCase
    ) {
        this.registrationUseCase = registrationUseCase;
        this.findUseCase = findUseCase;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Mono<CandidateForm> registry(@RequestBody final NewCandidateForm form) {
        return registrationUseCase.register(form.toModel()).map(CandidateForm::fromModel);
    }

    @GetMapping("/{name}")
    @ResponseStatus(HttpStatus.OK)
    Mono<CandidateForm> findByName(@PathVariable("name") final String name) {
        return findUseCase.findByName(name).map(CandidateForm::fromModel);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Flux<CandidateForm> findAll() {
        return findUseCase.findAll().map(CandidateForm::fromModel);
    }
}
