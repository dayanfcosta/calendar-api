package com.dayanfcosta.challenges.calendar.rest.shared;

import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import com.dayanfcosta.challenges.calendar.rest.config.Views;
import com.fasterxml.jackson.annotation.JsonView;
import java.time.LocalDateTime;

public class InterviewSlotForm {


    @JsonView({Views.Register.class, Views.List.class})
    private LocalDateTime start;

    @JsonView({Views.Register.class, Views.List.class})
    private LocalDateTime end;

    @JsonView({Views.Register.class, Views.List.class})
    private boolean busy;

    InterviewSlotForm() {
    }

    public InterviewSlotForm(final LocalDateTime start, final LocalDateTime end, final boolean busy) {
        this.start = start;
        this.end = end;
        this.busy = busy;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public static InterviewSlotForm fromModel(final InterviewSlot slot) {
        return new InterviewSlotForm(slot.getStart(), slot.getEnd(), slot.isBusy());
    }
}
