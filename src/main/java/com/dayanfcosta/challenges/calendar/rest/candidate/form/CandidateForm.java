package com.dayanfcosta.challenges.calendar.rest.candidate.form;

import com.dayanfcosta.challenges.calendar.domain.candidate.Candidate;
import com.dayanfcosta.challenges.calendar.rest.config.Views;
import com.dayanfcosta.challenges.calendar.rest.shared.InterviewSlotForm;
import com.fasterxml.jackson.annotation.JsonView;
import java.util.List;
import java.util.stream.Collectors;

public class CandidateForm {

    @JsonView({Views.Register.class, Views.List.class})
    private String id;
    @JsonView({Views.Register.class, Views.List.class})
    private String name;
    @JsonView({Views.Register.class, Views.List.class})
    private List<InterviewSlotForm> slots;

    CandidateForm() {
    }

    public CandidateForm(final String id, final String name, final List<InterviewSlotForm> slots) {
        this.id = id;
        this.name = name;
        this.slots = slots;
    }

    public static CandidateForm fromModel(final Candidate candidate) {
        return new CandidateForm(
                candidate.getId(),
                candidate.getName(),
                candidate.getSlots().stream()
                        .map(InterviewSlotForm::fromModel)
                        .collect(Collectors.toUnmodifiableList())
        );
    }
}
