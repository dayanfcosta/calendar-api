package com.dayanfcosta.challenges.calendar.rest.interviewer;

import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.rest.interviewer.form.InterviewerForm;
import com.dayanfcosta.challenges.calendar.rest.interviewer.form.NewInterviewerForm;
import com.dayanfcosta.challenges.calendar.usecase.interviewer.FindInterviewer;
import com.dayanfcosta.challenges.calendar.usecase.interviewer.InterviewerRegistration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/interviewers", consumes = MediaType.APPLICATION_JSON_VALUE)
class InterviewerController {

    private final InterviewerRegistration registryUseCase;
    private final FindInterviewer findUseCase;

    public InterviewerController(final InterviewerRegistration registryUseCase, final FindInterviewer findUseCase) {
        this.registryUseCase = registryUseCase;
        this.findUseCase = findUseCase;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Mono<Interviewer> registry(@RequestBody final NewInterviewerForm form) {
        final var interviewer = form.toModel();
        return registryUseCase.register(interviewer);
    }

    @GetMapping("/{name}")
    @ResponseStatus(HttpStatus.OK)
    Mono<InterviewerForm> findByName(@PathVariable("name") final String name) {
        return findUseCase.findByName(name).map(InterviewerForm::fromModel);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Flux<InterviewerForm> findAll() {
        return findUseCase.findAll().map(InterviewerForm::fromModel);
    }
}
