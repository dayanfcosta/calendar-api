package com.dayanfcosta.challenges.calendar.rest.interview.form;

import com.dayanfcosta.challenges.calendar.domain.interview.Interview;
import com.dayanfcosta.challenges.calendar.rest.interview.InterviewViews;
import com.dayanfcosta.challenges.calendar.rest.shared.InterviewSlotForm;
import com.fasterxml.jackson.annotation.JsonView;

public class InterviewForm {

    @JsonView({
            InterviewViews.Register.class,
            InterviewViews.List.class,
            InterviewViews.ListInterviewer.class,
            InterviewViews.ListCandidate.class
    })
    private String id;

    @JsonView({
            InterviewViews.Register.class,
            InterviewViews.ListInterviewer.class
    })
    private String candidate;

    @JsonView({
            InterviewViews.Register.class,
            InterviewViews.ListCandidate.class
    })
    private String interviewer;

    @JsonView({
            InterviewViews.Register.class,
            InterviewViews.List.class,
            InterviewViews.ListInterviewer.class,
            InterviewViews.ListCandidate.class
    })
    private InterviewSlotForm slot;

    InterviewForm() {
    }

    public InterviewForm(final String id, final String candidate, final String interviewer,
            final InterviewSlotForm slot) {
        this.id = id;
        this.candidate = candidate;
        this.interviewer = interviewer;
        this.slot = slot;
    }

    public String getId() {
        return id;
    }

    public String getCandidate() {
        return candidate;
    }

    public String getInterviewer() {
        return interviewer;
    }

    public InterviewSlotForm getSlot() {
        return slot;
    }

    public static InterviewForm fromModel(final Interview interview) {
        return new InterviewForm(
                interview.getId(),
                interview.getCandidate().getName(),
                interview.getInterviewer().getName(),
                InterviewSlotForm.fromModel(interview.getSlot())
        );
    }
}
