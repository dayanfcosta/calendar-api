package com.dayanfcosta.challenges.calendar.usecase.interviewer;

import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerRepository;
import java.util.NoSuchElementException;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class FindInterviewer {

    private final InterviewerRepository interviewerRepository;

    public FindInterviewer(final InterviewerRepository interviewerRepository) {
        this.interviewerRepository = interviewerRepository;
    }

    public Mono<Interviewer> findByName(final String name) {
        Validate.notBlank(name, "Candidate name invalid");
        return interviewerRepository.findByName(name)
                .switchIfEmpty(Mono.error(new NoSuchElementException("Interviewer not found")));
    }

    public Flux<Interviewer> findAll() {
        return interviewerRepository.findAll();
    }
}
