package com.dayanfcosta.challenges.calendar.usecase.candidate;

import com.dayanfcosta.challenges.calendar.domain.candidate.Candidate;
import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateBuilder;
import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateRepository;
import com.dayanfcosta.challenges.calendar.usecase.shared.InterviewSlotUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class CandidateRegistration {

    private final CandidateRepository repository;

    public CandidateRegistration(final CandidateRepository repository) {
        this.repository = repository;
    }

    public Mono<Candidate> register(final Candidate candidate) {
        Validate.notNull(candidate, "Invalid candidate");
        final var newCandidate = new CandidateBuilder()
                .withName(candidate.getName())
                .withSlots(InterviewSlotUtils.slotsInPeriods(candidate.getSlots()))
                .build();
        return repository.register(newCandidate);
    }
}
