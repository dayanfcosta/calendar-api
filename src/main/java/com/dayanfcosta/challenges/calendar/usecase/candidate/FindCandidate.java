package com.dayanfcosta.challenges.calendar.usecase.candidate;

import com.dayanfcosta.challenges.calendar.domain.candidate.Candidate;
import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateRepository;
import java.util.NoSuchElementException;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class FindCandidate {

    private final CandidateRepository candidateRepository;

    public FindCandidate(final CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    public Mono<Candidate> findByName(final String name) {
        Validate.notBlank(name, "Candidate name invalid");
        return candidateRepository.findByName(name)
                .switchIfEmpty(Mono.error(new NoSuchElementException("Candidate not found")));
    }

    public Flux<Candidate> findAll() {
        return candidateRepository.findAll();
    }
}
