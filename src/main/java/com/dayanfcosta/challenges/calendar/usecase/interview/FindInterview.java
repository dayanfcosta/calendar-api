package com.dayanfcosta.challenges.calendar.usecase.interview;

import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateRepository;
import com.dayanfcosta.challenges.calendar.domain.interview.Interview;
import com.dayanfcosta.challenges.calendar.domain.interview.InterviewRepository;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class FindInterview {

    private final InterviewRepository interviewRepository;
    private final CandidateRepository candidateRepository;
    private final InterviewerRepository interviewerRepository;

    public FindInterview(final InterviewRepository interviewRepository,
            final CandidateRepository candidateRepository,
            final InterviewerRepository interviewerRepository) {
        this.interviewRepository = interviewRepository;
        this.candidateRepository = candidateRepository;
        this.interviewerRepository = interviewerRepository;
    }

    public Flux<Interview> findByInterviewer(final String interviewerName) {
        return interviewerRepository.findByName(interviewerName).flux()
                .flatMap(interviewer -> interviewRepository.findByInterviewer(interviewer.getId()));
    }

    public Flux<Interview> findByCandidate(final String candidateName) {
        return candidateRepository.findByName(candidateName).flux()
                .flatMap(candidate -> interviewRepository.findByCandidate(candidate.getId()));
    }
}
