package com.dayanfcosta.challenges.calendar.usecase.shared;

import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.IntStream;
import org.apache.commons.lang3.Validate;

public final class InterviewSlotUtils {

    public static Set<InterviewSlot> slotsInPeriods(final Set<InterviewSlot> slots) {
        final var newSlots = new LinkedHashSet<InterviewSlot>();
        slots.forEach(slot -> {
            final var period = ChronoUnit.HOURS.between(slot.getStart(), slot.getEnd());
            Validate.isTrue(period >= 1,
                    "Slot between %s and %s invalid", slot.getStart(), slot.getEnd());

            if (period == 1) {
                newSlots.add(slot);
            } else {
                IntStream.iterate(0, hour -> hour + 1)
                        .limit(period)
                        .mapToObj(hour -> {
                            final var slotStart = slot.getStart().plusHours(hour);
                            return new InterviewSlot(slotStart, slotStart.plusHours(1), false);
                        })
                        .forEach(newSlots::add);
            }
        });
        return Collections.unmodifiableSet(newSlots);
    }
}
