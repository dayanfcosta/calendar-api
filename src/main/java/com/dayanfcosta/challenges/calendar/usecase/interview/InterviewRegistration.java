package com.dayanfcosta.challenges.calendar.usecase.interview;

import com.dayanfcosta.challenges.calendar.domain.candidate.Candidate;
import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateRepository;
import com.dayanfcosta.challenges.calendar.domain.interview.Interview;
import com.dayanfcosta.challenges.calendar.domain.interview.InterviewBuilder;
import com.dayanfcosta.challenges.calendar.domain.interview.InterviewRepository;
import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerRepository;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import com.dayanfcosta.challenges.calendar.infrastructure.config.DuplicatedKeyException;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class InterviewRegistration {

    private final InterviewRepository interviewRepository;
    private final CandidateRepository candidateRepository;
    private final InterviewerRepository interviewerRepository;

    public InterviewRegistration(
            final InterviewRepository interviewRepository,
            final CandidateRepository candidateRepository,
            final InterviewerRepository interviewerRepository
    ) {
        this.interviewRepository = interviewRepository;
        this.candidateRepository = candidateRepository;
        this.interviewerRepository = interviewerRepository;
    }

    @DuplicatedKeyException("This interview is already registered")
    public Mono<Interview> register(final Interview interview) {
        Validate.notNull(interview, "Invalid interview");
        return Mono.zip(
                scheduleSlotForCandidate(interview.getCandidate(), interview.getSlot()),
                scheduleSlotForInterviewer(interview.getInterviewer(), interview.getSlot()),
                (candidate, interviewer) -> new InterviewBuilder()
                        .withCandidate(candidate)
                        .withInterviewer(interviewer)
                        .withSlot(interview.getSlot())
                        .build()
        ).flatMap(interviewRepository::register);
    }


    private Mono<Interviewer> scheduleSlotForInterviewer(final Interviewer interviewerName, final InterviewSlot slot) {
        return interviewerRepository.findByName(interviewerName.getName())
                .filter(interviewer -> interviewer.getAvailableSlots().contains(slot))
                .flatMap(interviewer -> interviewerRepository.markSlotAsUsed(interviewer.getId(), slot))
                .switchIfEmpty(error("Interviewer does not contain this slot available"));
    }

    private Mono<Candidate> scheduleSlotForCandidate(final Candidate candidateName, final InterviewSlot slot) {
        return candidateRepository.findByName(candidateName.getName())
                .filter(candidate -> candidate.getAvailableSlots().contains(slot))
                .flatMap(candidate -> candidateRepository.markSlotAsUsed(candidate.getId(), slot))
                .switchIfEmpty(error("Candidate does not contain this slot available"));
    }

    private Mono error(final String message) {
        return Mono.error(new IllegalArgumentException(message));
    }
}
