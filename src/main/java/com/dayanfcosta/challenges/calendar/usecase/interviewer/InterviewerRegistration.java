package com.dayanfcosta.challenges.calendar.usecase.interviewer;

import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerBuilder;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerRepository;
import com.dayanfcosta.challenges.calendar.infrastructure.config.DuplicatedKeyException;
import com.dayanfcosta.challenges.calendar.usecase.shared.InterviewSlotUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class InterviewerRegistration {

    private final InterviewerRepository repository;

    public InterviewerRegistration(final InterviewerRepository repository) {
        this.repository = repository;
    }

    @DuplicatedKeyException("Interviewer already registered with the given name")
    public Mono<Interviewer> register(final Interviewer interviewer) {
        Validate.notNull(interviewer, "Invalid interviewer");
        final var newInterviewer = new InterviewerBuilder()
                .withName(interviewer.getName())
                .withSlots(InterviewSlotUtils.slotsInPeriods(interviewer.getSlots()))
                .build();
        return repository.register(newInterviewer);
    }
}
