package com.dayanfcosta.challenges.calendar.infrastructure.interviewer;

import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerBuilder;
import com.dayanfcosta.challenges.calendar.infrastructure.shared.AbstractDocument;
import com.dayanfcosta.challenges.calendar.infrastructure.slot.InterviewSlotDocument;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "interviewers")
public class InterviewerDocument extends AbstractDocument {

    @Indexed(unique = true, name = "uk_interviewer_name")
    private final String name;
    private final List<InterviewSlotDocument> slots;

    @PersistenceConstructor
    public InterviewerDocument(final String id, final String name,
            final List<InterviewSlotDocument> slots) {
        super(id);
        this.name = name;
        this.slots = slots;
    }

    String getName() {
        return name;
    }

    List<InterviewSlotDocument> getSlots() {
        return Collections.unmodifiableList(slots);
    }

    public Interviewer toModel() {
        final var slots = getSlots().stream()
                .map(InterviewSlotDocument::toModel)
                .collect(Collectors.toCollection(LinkedHashSet::new));
        return new InterviewerBuilder()
                .withId(getId())
                .withName(getName())
                .withSlots(slots)
                .build();
    }

    public static InterviewerDocument from(final Interviewer interviewer) {
        final var slots = interviewer.getSlots().stream()
                .map(InterviewSlotDocument::from)
                .collect(Collectors.toList());
        return new InterviewerDocument(interviewer.getId(), interviewer.getName(), slots);
    }
}
