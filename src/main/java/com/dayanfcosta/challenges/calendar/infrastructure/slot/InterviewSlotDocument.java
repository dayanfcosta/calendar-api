package com.dayanfcosta.challenges.calendar.infrastructure.slot;

import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import java.time.LocalDateTime;
import org.springframework.data.annotation.PersistenceConstructor;

public class InterviewSlotDocument {

    private final LocalDateTime start;
    private final LocalDateTime end;
    private final boolean busy;

    @PersistenceConstructor
    public InterviewSlotDocument(final LocalDateTime start, final LocalDateTime end,
            final boolean busy) {
        this.start = start;
        this.end = end;
        this.busy = busy;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    boolean isBusy() {
        return busy;
    }

    public InterviewSlot toModel() {
        return new InterviewSlot(start, end, busy);
    }

    public static InterviewSlotDocument from(final InterviewSlot slot) {
        return new InterviewSlotDocument(slot.getStart(), slot.getEnd(), slot.isBusy());
    }
}
