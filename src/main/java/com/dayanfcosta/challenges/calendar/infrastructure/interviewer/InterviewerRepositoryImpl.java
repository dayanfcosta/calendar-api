package com.dayanfcosta.challenges.calendar.infrastructure.interviewer;

import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerRepository;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import com.dayanfcosta.challenges.calendar.infrastructure.shared.AbstractRepository;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public class InterviewerRepositoryImpl extends AbstractRepository<InterviewerDocument> implements
        InterviewerRepository {

    public InterviewerRepositoryImpl(final ReactiveMongoTemplate mongoTemplate) {
        super(InterviewerDocument.class, mongoTemplate);
    }

    @Override
    public Mono<Interviewer> register(final Interviewer interviewer) {
        return save(InterviewerDocument.from(interviewer))
                .map(InterviewerDocument::toModel);
    }

    @Override
    public Mono<Interviewer> findByName(final String name) {
        final var query = new Query(Criteria.where("name").is(name));
        return findOne(query).map(InterviewerDocument::toModel);
    }

    @Override
    public Mono<Interviewer> markSlotAsUsed(final String id, final InterviewSlot slot) {
        final var query = new Query(
                Criteria.where("id").is(id)
                        .and("slots.start").is(slot.getStart())
                        .and("slots.end").is(slot.getEnd())
        );

        final var update = new Update().set("slots.$.busy", true);
        return getMongoTemplate().findAndModify(query, update, InterviewerDocument.class)
                .then(findById(id));
    }

    @Override
    public Flux<Interviewer> findAll() {
        return getMongoTemplate().findAll(InterviewerDocument.class)
                .map(InterviewerDocument::toModel);
    }

    @Override
    public Mono<Interviewer> findById(final String id) {
        return getMongoTemplate().findById(id, InterviewerDocument.class)
                .map(InterviewerDocument::toModel);
    }
}
