package com.dayanfcosta.challenges.calendar.infrastructure.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Aspect
@Component
public class DuplicatedKeyAspect {

    @Around("@annotation(com.dayanfcosta.challenges.calendar.infrastructure.config.DuplicatedKeyException)")
    public Object arroundDuplicatedKey(final ProceedingJoinPoint joinPoint) throws Throwable {
        return ((Mono<Object>) joinPoint.proceed())
                .doOnError(
                        throwable -> throwable instanceof DataIntegrityViolationException,
                        throwable -> {
                            final var signature = (MethodSignature) joinPoint.getSignature();
                            final var method = signature.getMethod();
                            final var message = method.getAnnotation(DuplicatedKeyException.class).value();
                            throw new DataIntegrityViolationException(message);
                        }
                );
    }

}
