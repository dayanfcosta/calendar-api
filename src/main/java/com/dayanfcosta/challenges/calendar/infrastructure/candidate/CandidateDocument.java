package com.dayanfcosta.challenges.calendar.infrastructure.candidate;

import com.dayanfcosta.challenges.calendar.domain.candidate.Candidate;
import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateBuilder;
import com.dayanfcosta.challenges.calendar.infrastructure.shared.AbstractDocument;
import com.dayanfcosta.challenges.calendar.infrastructure.slot.InterviewSlotDocument;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "candidates")
public class CandidateDocument extends AbstractDocument {

    @Indexed(unique = true, name = "uk_candidate_name")
    private final String name;
    private final List<InterviewSlotDocument> slots;

    @PersistenceConstructor
    public CandidateDocument(final String id, final String name,
            final List<InterviewSlotDocument> slots) {
        super(id);
        this.name = name;
        this.slots = slots;
    }

    public String getName() {
        return name;
    }

    public List<InterviewSlotDocument> getSlots() {
        return Collections.unmodifiableList(slots);
    }

    public Candidate toModel() {
        final var slots = getSlots().stream()
                .map(InterviewSlotDocument::toModel)
                .collect(Collectors.toCollection(LinkedHashSet::new));
        return new CandidateBuilder()
                .withId(getId())
                .withName(getName())
                .withSlots(slots)
                .build();
    }

    public static CandidateDocument from(final Candidate interviewer) {
        final var slots = interviewer.getSlots().stream()
                .map(InterviewSlotDocument::from)
                .collect(Collectors.toList());
        return new CandidateDocument(interviewer.getId(), interviewer.getName(), slots);
    }
}
