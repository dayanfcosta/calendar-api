package com.dayanfcosta.challenges.calendar.infrastructure.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DuplicatedKeyException {

    /**
     * Indicates the message that will be shown on logs
     *
     * @return message to be logged
     */
    String value() default "";

}
