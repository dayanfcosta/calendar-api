package com.dayanfcosta.challenges.calendar.infrastructure.interview;

import com.dayanfcosta.challenges.calendar.domain.candidate.Candidate;
import com.dayanfcosta.challenges.calendar.domain.interview.Interview;
import com.dayanfcosta.challenges.calendar.domain.interview.InterviewBuilder;
import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.infrastructure.shared.AbstractDocument;
import com.dayanfcosta.challenges.calendar.infrastructure.slot.InterviewSlotDocument;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "interviews")
@CompoundIndex(
        unique = true,
        name = "uk_interview",
        def = "{'interviewer': 1, 'candidate': 1, 'slot': 1}"
)
class InterviewDocument extends AbstractDocument {

    private final String interviewerId;
    private final String candidateId;
    private final InterviewSlotDocument slot;

    @PersistenceConstructor
    public InterviewDocument(final String id, final String interviewerId,
            final String candidateId, final InterviewSlotDocument slot) {
        super(id);
        this.interviewerId = interviewerId;
        this.candidateId = candidateId;
        this.slot = slot;
    }

    public String getInterviewerId() {
        return interviewerId;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public InterviewSlotDocument getSlot() {
        return slot;
    }

    Interview toModel(final Interviewer interviewer, final Candidate candidate) {
        return new InterviewBuilder()
                .withId(getId())
                .withInterviewer(interviewer)
                .withCandidate(candidate)
                .withSlot(getSlot().toModel())
                .build();
    }

    static InterviewDocument from(final Interview interview) {
        return new InterviewDocument(
                interview.getId(),
                interview.getInterviewer().getId(),
                interview.getCandidate().getId(),
                InterviewSlotDocument.from(interview.getSlot())
        );
    }
}
