package com.dayanfcosta.challenges.calendar.infrastructure.candidate;

import com.dayanfcosta.challenges.calendar.domain.candidate.Candidate;
import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateRepository;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import com.dayanfcosta.challenges.calendar.infrastructure.shared.AbstractRepository;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public class CandidateRepositoryImpl extends AbstractRepository<CandidateDocument> implements
        CandidateRepository {

    public CandidateRepositoryImpl(final ReactiveMongoTemplate mongoTemplate) {
        super(CandidateDocument.class, mongoTemplate);
    }

    @Override
    public Mono<Candidate> register(final Candidate candidate) {
        return save(CandidateDocument.from(candidate))
                .map(CandidateDocument::toModel);
    }

    @Override
    public Mono<Candidate> findByName(final String name) {
        final var query = new Query(Criteria.where("name").is(name));
        return findOne(query).map(CandidateDocument::toModel);
    }

    @Override
    public Mono<Candidate> markSlotAsUsed(final String id, final InterviewSlot slot) {
        final var query = new Query(
                Criteria.where("id").is(id)
                        .and("slots.start").is(slot.getStart())
                        .and("slots.end").is(slot.getEnd())
        );

        final var update = new Update().set("slots.$.busy", true);
        return getMongoTemplate().findAndModify(query, update, CandidateDocument.class)
                .then(findById(id));
    }

    @Override
    public Flux<Candidate> findAll() {
        return getMongoTemplate().findAll(CandidateDocument.class)
                .map(CandidateDocument::toModel);
    }

    @Override
    public Mono<Candidate> findById(final String id) {
        return getMongoTemplate().findById(id, CandidateDocument.class)
                .map(CandidateDocument::toModel);
    }
}
