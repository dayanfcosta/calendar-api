package com.dayanfcosta.challenges.calendar.infrastructure.shared;

import org.apache.commons.lang3.Validate;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public abstract class AbstractRepository<D extends AbstractDocument> {

    private final Class<D> clazz;
    private final ReactiveMongoTemplate mongoTemplate;

    public AbstractRepository(final Class<D> clazz, final ReactiveMongoTemplate mongoTemplate) {
        this.clazz = Validate.notNull(clazz, "Invalid repository class");
        this.mongoTemplate = mongoTemplate;
    }

    protected Mono<D> save(final D document) {
        return mongoTemplate.save(document);
    }

    protected Mono<Boolean> remove(final D document) {
        return mongoTemplate.remove(document).single().map(result -> result.getDeletedCount() > 0);
    }

    protected Flux<D> find(final Query query) {
        return mongoTemplate.find(query, clazz);
    }

    protected Mono<D> findOne(final Query query) {
        return mongoTemplate.findOne(query, clazz);
    }

    protected ReactiveMongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }
}
