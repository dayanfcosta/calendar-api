package com.dayanfcosta.challenges.calendar.infrastructure.interview;

import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateRepository;
import com.dayanfcosta.challenges.calendar.domain.interview.Interview;
import com.dayanfcosta.challenges.calendar.domain.interview.InterviewRepository;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerRepository;
import com.dayanfcosta.challenges.calendar.infrastructure.shared.AbstractRepository;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
class InterviewRepositoryImpl extends AbstractRepository<InterviewDocument>
        implements InterviewRepository {

    private final CandidateRepository candidateRepository;
    private final InterviewerRepository interviewerRepository;

    public InterviewRepositoryImpl(
            final ReactiveMongoTemplate mongoTemplate,
            final CandidateRepository candidateRepository,
            final InterviewerRepository interviewerRepository
    ) {
        super(InterviewDocument.class, mongoTemplate);
        this.candidateRepository = candidateRepository;
        this.interviewerRepository = interviewerRepository;
    }

    @Override
    public Mono<Interview> register(final Interview interview) {
        return save(InterviewDocument.from(interview))
                .map(document -> document.toModel(interview.getInterviewer(), interview.getCandidate()));
    }

    @Override
    public Flux<Interview> findByCandidate(final String candidateId) {
        final var query = new Query(Criteria.where("candidateId").is(candidateId));
        return find(query)
                .flatMap(document -> interviewerRepository.findById(document.getInterviewerId())
                        .zipWith(candidateRepository.findById(candidateId), document::toModel).flux()
                );
    }

    @Override
    public Flux<Interview> findByInterviewer(final String interviewerId) {
        final var query = new Query(Criteria.where("interviewerId").is(interviewerId));
        return find(query)
                .flatMap(document -> interviewerRepository.findById(interviewerId)
                        .zipWith(candidateRepository.findById(document.getCandidateId()), document::toModel).flux()
                );
    }

}
