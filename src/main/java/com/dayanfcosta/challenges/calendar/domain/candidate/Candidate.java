package com.dayanfcosta.challenges.calendar.domain.candidate;

import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Candidate {

    private final String id;
    private final String name;
    private final Set<InterviewSlot> slots;

    Candidate(final String id, final String name, final Set<InterviewSlot> slots) {
        this.id = id;
        this.name = name;
        this.slots = slots;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<InterviewSlot> getSlots() {
        return Collections.unmodifiableSet(slots);
    }

    public Set<InterviewSlot> getAvailableSlots() {
        return getSlots().stream()
                .filter(slot -> !slot.isBusy())
                .collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Candidate candidate = (Candidate) o;
        return name.equals(candidate.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .toString();
    }
}
