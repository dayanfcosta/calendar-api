package com.dayanfcosta.challenges.calendar.domain.interview;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface InterviewRepository {

    Mono<Interview> register(Interview interview);

    Flux<Interview> findByCandidate(String candidateId);

    Flux<Interview> findByInterviewer(String interviewerId);
}
