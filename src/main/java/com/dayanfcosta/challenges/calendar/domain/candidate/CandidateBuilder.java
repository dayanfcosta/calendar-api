package com.dayanfcosta.challenges.calendar.domain.candidate;

import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import java.util.LinkedHashSet;
import java.util.Set;
import org.apache.commons.lang3.Validate;

public class CandidateBuilder {

    private String id;
    private String name;
    private Set<InterviewSlot> slots = new LinkedHashSet<>();

    public CandidateBuilder() {
        super();
    }

    public CandidateBuilder withId(final String id) {
        this.id = Validate.notBlank(id, "Invalid candidate ID");
        return this;
    }

    public CandidateBuilder withName(final String name) {
        this.name = Validate.notBlank(name, "Invalid candidate name");
        return this;
    }

    public CandidateBuilder withSlots(final Set<InterviewSlot> slots) {
        this.slots = slots;
        return this;
    }

    public CandidateBuilder addSlot(final InterviewSlot slot) {
        Validate.notNull(slot, "Invalid interview slot");
        slots.add(slot);
        return this;
    }

    public Candidate build() {
        return new Candidate(id, name, slots);
    }
}
