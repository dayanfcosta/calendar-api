package com.dayanfcosta.challenges.calendar.domain.candidate;

import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CandidateRepository {

    Mono<Candidate> register(Candidate candidate);

    Mono<Candidate> findByName(String name);

    Mono<Candidate> markSlotAsUsed(String id, InterviewSlot slot);

    Flux<Candidate> findAll();

    Mono<Candidate> findById(String id);
}
