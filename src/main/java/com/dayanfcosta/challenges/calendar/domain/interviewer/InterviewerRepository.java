package com.dayanfcosta.challenges.calendar.domain.interviewer;

import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface InterviewerRepository {

    Mono<Interviewer> register(Interviewer interviewer);

    Mono<Interviewer> findByName(String name);

    Mono<Interviewer> markSlotAsUsed(String id, InterviewSlot slot);

    Flux<Interviewer> findAll();

    Mono<Interviewer> findById(String id);
}
