package com.dayanfcosta.challenges.calendar.domain.slot;

import java.time.LocalDateTime;
import java.util.Objects;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class InterviewSlot implements Comparable {

    private final LocalDateTime start;
    private final LocalDateTime end;
    private boolean busy;

    public InterviewSlot(final LocalDateTime start, final LocalDateTime end, final boolean busy) {
        validateSlot(start, end);
        this.start = start;
        this.end = end;
        this.busy = busy;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public boolean isBusy() {
        return busy;
    }

    public void use() {
        busy = true;
    }

    public void makeFree() {
        busy = false;
    }

    private static void validateSlot(final LocalDateTime start, final LocalDateTime end) {
        Validate.notNull(start, "Invalid interview slot start");
        Validate.notNull(end, "Invalid interview slot end");
        Validate.isTrue(start.isBefore(end), "Invalid interview slot period");
        Validate.isTrue(start.getMinute() == 0, "Interview slot start must be in exact hour, e.g 9:00");
        Validate.isTrue(end.getMinute() == 0, "Interview slot end must be in exact hour, e.g 9:00");
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("from", start)
                .append("to", end)
                .toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final InterviewSlot that = (InterviewSlot) o;
        return start.equals(that.start) && end.equals(that.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    @Override
    public int compareTo(final Object o) {
        final var slot = (InterviewSlot) o;
        return start.compareTo(slot.getStart());
    }
}
