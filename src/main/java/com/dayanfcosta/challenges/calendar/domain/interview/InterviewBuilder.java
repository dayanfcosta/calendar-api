package com.dayanfcosta.challenges.calendar.domain.interview;

import com.dayanfcosta.challenges.calendar.domain.candidate.Candidate;
import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import org.apache.commons.lang3.Validate;

public class InterviewBuilder {

    private String id;
    private InterviewSlot slot;
    private Candidate candidate;
    private Interviewer interviewer;

    public InterviewBuilder() {
        super();
    }

    public InterviewBuilder withSlot(final InterviewSlot slot) {
        Validate.notNull(slot, "Invalid interview slot");
        Validate.isTrue(!slot.isBusy(), "Interview slot already busy");
        this.slot = slot;
        return this;
    }

    public InterviewBuilder withCandidate(final Candidate candidate) {
        this.candidate = Validate.notNull(candidate, "Invalid candidate");
        return this;
    }

    public InterviewBuilder withInterviewer(final Interviewer interviewer) {
        this.interviewer = Validate.notNull(interviewer, "Invalid interviewer");
        return this;
    }

    public InterviewBuilder withId(final String id) {
        this.id = Validate.notBlank(id, "Invalid interview ID");
        return this;
    }

    public Interview build() {
        Validate.notNull(interviewer, "Invalid interviewer");
        Validate.notNull(candidate, "Invalid candidate");
        Validate.notNull(slot, "Invalid interview slot");
        Validate.isTrue(!slot.isBusy(), "Interview slot already busy");
        return new Interview(id, interviewer, candidate, slot);
    }
}
