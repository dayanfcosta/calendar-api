package com.dayanfcosta.challenges.calendar.domain.interviewer;

import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import java.util.LinkedHashSet;
import java.util.Set;
import org.apache.commons.lang3.Validate;

public class InterviewerBuilder {

    private String id;
    private String name;
    private Set<InterviewSlot> slots = new LinkedHashSet<>();

    public InterviewerBuilder() {
        super();
    }

    public InterviewerBuilder withId(final String id) {
        this.id = Validate.notBlank(id, "Invalid interviewer ID");
        return this;
    }

    public InterviewerBuilder withName(final String name) {
        this.name = Validate.notBlank(name, "Invalid interviewer name");
        return this;
    }

    public InterviewerBuilder withSlots(final Set<InterviewSlot> slots) {
        this.slots = slots;
        return this;
    }

    public InterviewerBuilder addSlot(final InterviewSlot slot) {
        Validate.notNull(slot, "Invalid interview slot");
        slots.add(slot);
        return this;
    }

    public Interviewer build() {
        return new Interviewer(id, name, slots);
    }

}
