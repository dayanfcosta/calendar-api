package com.dayanfcosta.challenges.calendar.domain.interview;

import com.dayanfcosta.challenges.calendar.domain.candidate.Candidate;
import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import java.util.Objects;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Interview {

    private final String id;
    private final Interviewer interviewer;
    private final Candidate candidate;
    private final InterviewSlot slot;

    Interview(final String id, final Interviewer interviewer, final Candidate candidate,
            final InterviewSlot slot) {
        this.id = id;
        this.interviewer = interviewer;
        this.candidate = candidate;
        this.slot = slot;
    }

    public String getId() {
        return id;
    }

    public Interviewer getInterviewer() {
        return interviewer;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public InterviewSlot getSlot() {
        return slot;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Interview interview = (Interview) o;
        return interviewer.equals(interview.interviewer)
                && candidate.equals(interview.candidate)
                && slot.equals(interview.slot);
    }

    @Override
    public int hashCode() {
        return Objects.hash(interviewer, candidate, slot);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("interviewer", interviewer.getName())
                .append("candidate", candidate.getName())
                .append("slot", slot)
                .toString();
    }
}
