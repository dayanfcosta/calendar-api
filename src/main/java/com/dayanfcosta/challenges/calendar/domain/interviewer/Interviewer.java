package com.dayanfcosta.challenges.calendar.domain.interviewer;

import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Interviewer {

    private final String id;
    private final String name;
    private final Set<InterviewSlot> slots;

    Interviewer(final String id, final String name, final Set<InterviewSlot> slots) {
        this.id = id;
        this.name = name;
        this.slots = slots;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<InterviewSlot> getSlots() {
        return slots;
    }

    public Set<InterviewSlot> getAvailableSlots() {
        return slots.stream()
                .filter(slot -> !slot.isBusy())
                .collect(Collectors.toSet());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Interviewer that = (Interviewer) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .toString();
    }
}
