package com.dayanfcosta.challenges.calendar.domain.slot;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.junit.jupiter.api.Test;

class InterviewSlotTest {

  @Test
  void shouldThrowExceptionWhenSlotStartIsNull() {
    final var ex = assertThrows(NullPointerException.class,
        () -> new InterviewSlot(null, LocalDateTime.now(), false));

    assertEquals("Invalid interview slot start", ex.getMessage());
  }

  @Test
  void shouldThrowExceptionWhenSlotEndIsNull() {
    final var ex = assertThrows(NullPointerException.class,
        () -> new InterviewSlot(LocalDateTime.now(), null, false));

    assertEquals("Invalid interview slot end", ex.getMessage());
  }

  @Test
  void shouldThrowExceptionWhenSlotStartBeginsWithInvalidMinute() {
    final var start = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 45));
    final var end = LocalDateTime.of(LocalDate.now(), LocalTime.of(11, 0));

    final var ex = assertThrows(IllegalArgumentException.class,
        () -> new InterviewSlot(start, end, false));

    assertEquals("Interview slot start must be in exact hour, e.g 9:00", ex.getMessage());
  }

  @Test
  void shouldThrowExceptionWhenSlotEndBeginsWithInvalidMinute() {
    final var start = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 00));
    final var end = LocalDateTime.of(LocalDate.now(), LocalTime.of(11, 45));

    final var ex = assertThrows(IllegalArgumentException.class,
        () -> new InterviewSlot(start, end, false));

    assertEquals("Interview slot end must be in exact hour, e.g 9:00", ex.getMessage());
  }

  @Test
  void shouldCreateValidSlot() {
    final var start = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0));
    final var end = LocalDateTime.of(LocalDate.now().plusDays(10), LocalTime.of(18, 0));

    final var slot = new InterviewSlot(start, end, false);

    assertEquals(start, slot.getStart());
    assertEquals(end, slot.getEnd());
  }
}
