package com.dayanfcosta.challenges.calendar.infrastructure.candidate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateBuilder;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import com.dayanfcosta.challenges.calendar.infrastructure.AbstractRepositoryTest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CandidateRepositoryImplTest extends AbstractRepositoryTest {

  private CandidateRepositoryImpl repository;
  private InterviewSlot interviewSlot;

  public CandidateRepositoryImplTest() {
    addDocumentsToClear(CandidateDocument.class);
  }

  @BeforeEach
  public void setUp() {
    repository = new CandidateRepositoryImpl(getMongoTemplate());

    final var start = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0));
    final var end = LocalDateTime.of(LocalDate.now().plusDays(10), LocalTime.of(18, 0));

    interviewSlot = new InterviewSlot(start, end, false);
  }

  @Test
  void shouldRegisterCandidate() {
    final var interviewer = new CandidateBuilder()
        .withName("Test")
        .addSlot(interviewSlot)
        .build();

    final var newInterviewer = repository.register(interviewer).block();

    assertNotNull(newInterviewer);
    assertNotNull(newInterviewer.getId());
    assertEquals(interviewer.getName(), newInterviewer.getName());
    assertEquals(interviewer.getSlots().size(), newInterviewer.getSlots().size());
  }

  @Test
  void shouldReturnCandidateWithGivenName() {
    final var interviewer = new CandidateBuilder()
        .withName("Test")
        .addSlot(interviewSlot)
        .build();
    repository.register(interviewer).block();

    final var foundInterviewer = repository.findByName(interviewer.getName()).block();

    assertEquals(foundInterviewer, interviewer);
  }

  @Test
  void shouldUpdateSingleSlot() {
    final var start = LocalDateTime.of(LocalDate.now().plusDays(11), LocalTime.of(10, 0));
    final var end = LocalDateTime.of(LocalDate.now().plusDays(12), LocalTime.of(18, 0));
    final var slotToUpdate = new InterviewSlot(start, end, false);

    final var candidate = repository.register(
        new CandidateBuilder()
            .withName("Test")
            .addSlot(interviewSlot)
            .addSlot(slotToUpdate)
            .build()
    ).block();

    final var updated = repository.markSlotAsUsed(candidate.getId(), slotToUpdate).block();

    assertNotNull(updated);
    assertEquals(1, updated.getAvailableSlots().size());
  }
}
