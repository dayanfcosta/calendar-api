package com.dayanfcosta.challenges.calendar.infrastructure.interviewer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerBuilder;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import com.dayanfcosta.challenges.calendar.infrastructure.AbstractRepositoryTest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class InterviewerRepositoryImplTest extends AbstractRepositoryTest {

  private InterviewerRepositoryImpl repository;
  private InterviewSlot interviewSlot;

  public InterviewerRepositoryImplTest() {
    addDocumentsToClear(InterviewerDocument.class);
  }

  @BeforeEach
  public void setUp() {
    repository = new InterviewerRepositoryImpl(getMongoTemplate());

    final var start = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0));
    final var end = LocalDateTime.of(LocalDate.now().plusDays(10), LocalTime.of(18, 0));

    interviewSlot = new InterviewSlot(start, end, false);
  }

  @Test
  void shouldRegisterInterviewer() {
    final var interviewer = new InterviewerBuilder()
        .withName("Test")
        .addSlot(interviewSlot)
        .build();

    final var newInterviewer = repository.register(interviewer).block();

    assertNotNull(newInterviewer);
    assertNotNull(newInterviewer.getId());
    assertEquals(interviewer.getName(), newInterviewer.getName());
    assertEquals(interviewer.getSlots().size(), newInterviewer.getSlots().size());
  }

  @Test
  void shouldReturnInterviewerWithGivenName() {
    final var interviewer = new InterviewerBuilder()
        .withName("Test")
        .addSlot(interviewSlot)
        .build();
    repository.register(interviewer).block();

    final var foundInterviewer = repository.findByName(interviewer.getName()).block();

    assertEquals(foundInterviewer, interviewer);
  }

  @Test
  void shouldUpdateSingleSlot() {
    final var start = LocalDateTime.of(LocalDate.now().plusDays(11), LocalTime.of(10, 0));
    final var end = LocalDateTime.of(LocalDate.now().plusDays(12), LocalTime.of(18, 0));
    final var slotToUpdate = new InterviewSlot(start, end, false);

    final var interviewer = repository.register(
        new InterviewerBuilder()
            .withName("Test")
            .addSlot(interviewSlot)
            .addSlot(slotToUpdate)
            .build()
    ).block();

    final var updated = repository.markSlotAsUsed(interviewer.getId(), slotToUpdate).block();

    assertNotNull(updated);
    assertEquals(1, updated.getAvailableSlots().size());
  }
}
