package com.dayanfcosta.challenges.calendar.infrastructure;

import static java.util.Arrays.asList;

import com.dayanfcosta.challenges.calendar.infrastructure.config.InfrastructureConfig;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@DataMongoTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {InfrastructureConfig.class})
public class AbstractRepositoryTest {

    @Autowired
    private ReactiveMongoTemplate mongoTemplate;

    private final List<Class> documents = new ArrayList<>();

    @BeforeEach
    public void cleanDatabaseBefore() {
        documents.forEach(documentClass -> mongoTemplate.dropCollection(documentClass).block());
    }

    protected ReactiveMongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    protected void addDocumentsToClear(final Class... classes) {
        documents.addAll(asList(classes));
    }
}
