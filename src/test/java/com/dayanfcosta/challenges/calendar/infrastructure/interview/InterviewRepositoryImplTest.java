package com.dayanfcosta.challenges.calendar.infrastructure.interview;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.dayanfcosta.challenges.calendar.domain.candidate.Candidate;
import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateBuilder;
import com.dayanfcosta.challenges.calendar.domain.interview.Interview;
import com.dayanfcosta.challenges.calendar.domain.interview.InterviewBuilder;
import com.dayanfcosta.challenges.calendar.domain.interviewer.Interviewer;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerBuilder;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import com.dayanfcosta.challenges.calendar.infrastructure.AbstractRepositoryTest;
import com.dayanfcosta.challenges.calendar.infrastructure.candidate.CandidateDocument;
import com.dayanfcosta.challenges.calendar.infrastructure.candidate.CandidateRepositoryImpl;
import com.dayanfcosta.challenges.calendar.infrastructure.interviewer.InterviewerDocument;
import com.dayanfcosta.challenges.calendar.infrastructure.interviewer.InterviewerRepositoryImpl;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class InterviewRepositoryImplTest extends AbstractRepositoryTest {

    private InterviewRepositoryImpl interviewRepository;
    private CandidateRepositoryImpl candidateRepository;
    private InterviewerRepositoryImpl interviewerRepository;

    private InterviewSlot interviewSlot;
    private Interviewer interviewer;
    private Candidate candidate;
    private Interview interview;

    public InterviewRepositoryImplTest() {
        addDocumentsToClear(
                InterviewDocument.class,
                InterviewerDocument.class,
                CandidateDocument.class
        );
    }

    @BeforeEach
    void setUp() {
        candidateRepository = new CandidateRepositoryImpl(getMongoTemplate());
        interviewerRepository = new InterviewerRepositoryImpl(getMongoTemplate());
        interviewRepository = new InterviewRepositoryImpl(getMongoTemplate(), candidateRepository,
                interviewerRepository);

        final var start = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0));
        final var end = LocalDateTime.of(LocalDate.now().plusDays(10), LocalTime.of(18, 0));

        interviewSlot = new InterviewSlot(start, end, false);

        interviewer = interviewerRepository.register(
                new InterviewerBuilder()
                        .withName("Interviewer")
                        .addSlot(interviewSlot)
                        .build()
        ).block();

        candidate = candidateRepository.register(
                new CandidateBuilder()
                        .withName("Candidate")
                        .addSlot(interviewSlot)
                        .build()
        ).block();

        interview = new InterviewBuilder()
                .withCandidate(candidate)
                .withInterviewer(interviewer)
                .withSlot(interviewSlot)
                .build();
    }

    @Test
    void shouldRegisterInterview() {
        final var registeredInterview = interviewRepository.register(interview).block();

        assertNotNull(registeredInterview);
        assertEquals(interview, registeredInterview);
    }

    @Test
    void shouldFindInterviewsForCandidate() {
        interviewRepository.register(interview).block();

        final var interviews = interviewRepository.findByCandidate(candidate.getId()).collectList().block();

        assertEquals(1, interviews.size());
        assertEquals(interview, interviews.get(0));
    }

    @Test
    void shouldFindInterviewsForInterviewer() {
        interviewRepository.register(interview).block();

        final var interviews = interviewRepository.findByInterviewer(interviewer.getId()).collectList().block();

        assertEquals(1, interviews.size());
        assertEquals(interview, interviews.get(0));
    }
}
