package com.dayanfcosta.challenges.calendar.usecase.candidate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateBuilder;
import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateRepository;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import reactor.core.publisher.Mono;

class CandidateRegistrationTest {

  @Mock
  private CandidateRepository repository;

  private CandidateRegistration useCase;

  @BeforeEach
  void setUp() {
    openMocks(this);

    useCase = new CandidateRegistration(repository);
  }

  @Test
  void shouldThrowExceptionWhenRegisteringNullCandidate() {
    assertThrows(
        NullPointerException.class,
        () -> useCase.register(null)
    );
  }

  @Test
  void shouldRegisterCandidateAndCallRepository() {
    when(repository.register(any())).thenAnswer(answer -> {
      final var registering = answer.getArgument(0);
      return Mono.just(registering);
    });

    final var time = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0));
    final var candidate = new CandidateBuilder()
        .withName("Test")
        .addSlot(new InterviewSlot(time, time.plusHours(10), false))
        .build();

    final var registered = useCase.register(candidate).block();

    assertEquals(10, registered.getSlots().size());
    verify(repository).register(candidate);
  }
}
