package com.dayanfcosta.challenges.calendar.usecase.interviewer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerBuilder;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerRepository;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import reactor.core.publisher.Mono;

class InterviewerRegistryTest {

  @Mock
  private InterviewerRepository repository;

  private InterviewerRegistration useCase;

  @BeforeEach
  void setUp() {
    openMocks(this);

    useCase = new InterviewerRegistration(repository);
  }

  @Test
  void shouldThrowExceptionWhenRegisteringNullInterviewer() {
    assertThrows(
        NullPointerException.class,
        () -> useCase.register(null)
    );
  }

  @Test
  void shouldRegisterInterviewerAndCallRepository() {
    when(repository.register(any())).thenAnswer(answer -> {
      final var registering = answer.getArgument(0);
      return Mono.just(registering);
    });

    final var time = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0));
    final var interviewer = new InterviewerBuilder()
        .withName("Test")
        .addSlot(new InterviewSlot(time, time.plusHours(10), false))
        .build();

    final var registered = useCase.register(interviewer).block();

    assertEquals(10, registered.getSlots().size());
    verify(repository).register(interviewer);
  }
}
