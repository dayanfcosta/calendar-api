package com.dayanfcosta.challenges.calendar.usecase.candidate;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateBuilder;
import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateRepository;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import reactor.core.publisher.Mono;

class FindCandidateTest {

    @Mock
    private CandidateRepository candidateRepository;

    private FindCandidate useCase;

    @BeforeEach
    void setUp() {
        openMocks(this);

        useCase = new FindCandidate(candidateRepository);
    }

    @Test
    void shouldThrowExceptionWhenNameIsInvalid() {
        assertThatThrownBy(() -> useCase.findByName("").block())
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Candidate name invalid");

        assertThatThrownBy(() -> useCase.findByName(null).block())
                .isInstanceOf(NullPointerException.class)
                .hasMessage("Candidate name invalid");

        assertThatThrownBy(() -> useCase.findByName("     ").block())
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Candidate name invalid");
    }

    @Test
    void shouldThrowExceptionWhenCandidateIsNotFound() {
        when(candidateRepository.findByName(any())).thenReturn(Mono.empty());

        assertThatThrownBy(() -> useCase.findByName("Teste").block())
                .isInstanceOf(NoSuchElementException.class)
                .hasMessage("Candidate not found");
    }

    @Test
    void shouldReturnFoundCandidate() {
        final var time = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0));
        final var candidate = new CandidateBuilder()
                .withName("Test")
                .addSlot(new InterviewSlot(time, time.plusHours(1), false))
                .build();

        when(candidateRepository.findByName(any())).thenReturn(Mono.just(candidate));

        final var foundCandidate = useCase.findByName("Teste").block();

        verify(candidateRepository).findByName(any());
        assertNotNull(foundCandidate);
        assertEquals(candidate, foundCandidate);
    }
}
