package com.dayanfcosta.challenges.calendar.usecase.interview;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateBuilder;
import com.dayanfcosta.challenges.calendar.domain.candidate.CandidateRepository;
import com.dayanfcosta.challenges.calendar.domain.interview.InterviewBuilder;
import com.dayanfcosta.challenges.calendar.domain.interview.InterviewRepository;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerBuilder;
import com.dayanfcosta.challenges.calendar.domain.interviewer.InterviewerRepository;
import com.dayanfcosta.challenges.calendar.domain.slot.InterviewSlot;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import reactor.core.publisher.Mono;

class InterviewRegistrationTest {

    @Mock
    private InterviewRepository interviewRepository;
    @Mock
    private CandidateRepository candidateRepository;
    @Mock
    private InterviewerRepository interviewerRepository;

    private InterviewRegistration useCase;

    @BeforeEach
    void setUp() {
        openMocks(this);

        useCase = new InterviewRegistration(interviewRepository, candidateRepository, interviewerRepository);
    }

    @Test
    void shouldThrowExceptionWhenCandidateIsBusy() {
        final var busyCandidate = new CandidateBuilder().withId("1").withName("1").withSlots(busySlots()).build();
        final var availableInterviewer = new InterviewerBuilder().withId("1").withName("1").withSlots(availableSlots())
                .build();
        when(candidateRepository.findByName(any())).thenReturn(Mono.just(busyCandidate));
        when(interviewerRepository.findByName(any())).thenReturn(Mono.just(availableInterviewer));

        final var interview = new InterviewBuilder()
                .withInterviewer(availableInterviewer)
                .withCandidate(busyCandidate)
                .withSlot(availableSlots().stream().findFirst().orElseThrow())
                .build();

        assertThatThrownBy(() -> useCase.register(interview).block())
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Candidate does not contain this slot available");

        verify(interviewRepository, never()).register(any());
    }

    @Test
    void shouldThrowExceptionWhenInterviewerIsBusy() {
        final var availableCandidate = new CandidateBuilder().withId("1").withName("1").withSlots(availableSlots())
                .build();
        final var busyInterviewer = new InterviewerBuilder().withId("1").withName("1").withSlots(busySlots()).build();
        when(candidateRepository.findByName(any())).thenReturn(Mono.just(availableCandidate));
        when(interviewerRepository.findByName(any())).thenReturn(Mono.just(busyInterviewer));
        when(candidateRepository.markSlotAsUsed(any(), any())).thenReturn(Mono.just(availableCandidate));

        final var interview = new InterviewBuilder()
                .withInterviewer(busyInterviewer)
                .withCandidate(availableCandidate)
                .withSlot(availableSlots().stream().findFirst().orElseThrow())
                .build();

        assertThatThrownBy(() -> useCase.register(interview).block())
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Interviewer does not contain this slot available");

        verify(interviewRepository, never()).register(any());
    }

    @Test
    void shouldRegisterValidInterview() {
        final var availableCandidate = new CandidateBuilder().withId("1").withName("1").withSlots(availableSlots())
                .build();
        final var availableInterviewer = new InterviewerBuilder().withId("1").withName("1").withSlots(availableSlots())
                .build();
        when(candidateRepository.findByName(any())).thenReturn(Mono.just(availableCandidate));
        when(interviewerRepository.findByName(any())).thenReturn(Mono.just(availableInterviewer));
        when(candidateRepository.markSlotAsUsed(any(), any())).thenReturn(Mono.just(availableCandidate));
        when(interviewerRepository.markSlotAsUsed(any(), any())).thenReturn(Mono.just(availableInterviewer));

        final var interview = new InterviewBuilder()
                .withInterviewer(availableInterviewer)
                .withCandidate(availableCandidate)
                .withSlot(availableSlots().stream().findFirst().orElseThrow())
                .build();

        when(interviewRepository.register(any())).thenReturn(Mono.just(interview));

        useCase.register(interview).block();

        verify(interviewRepository).register(any());
    }

    private static Set<InterviewSlot> busySlots() {
        final var start = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0));
        return Set.of(
                new InterviewSlot(start, start.plusHours(1), true)
        );
    }

    private static Set<InterviewSlot> availableSlots() {
        final var start = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0));
        return Set.of(
                new InterviewSlot(start, start.plusHours(1), false)
        );
    }
}
